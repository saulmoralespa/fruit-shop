/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
	"shim": {
		"rokanthemes/lazyloadimg": ["jquery"]
	},
	'paths': {
		'rokanthemes/lazyloadimg': 'Rokanthemes_Themeoption/js/jquery.lazyload.min'
    }
};
